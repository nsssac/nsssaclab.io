+++
title = "Aleksi Toiviainen"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Aleksi Toiviainen"]
+++

Hi folks. I’m Aleksi Toiviainen, Northern’s Vice President for this year.
I chair the Senate and the elections committee.
My focus is on fixing the defects of the school government system:
making it more open, fair, and responsive to student needs.
Anyone with an idea for how Northern should be running deserves to be
confident that their input is being given the attention it deserves.
I’m very proud of the SAC team we have, and I know that despite
everything happening this year we’ll be able to change
our school for the better.
