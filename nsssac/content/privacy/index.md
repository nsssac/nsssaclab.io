+++
title = "Privacy Policy"
date = "1970-01-01T00:00:00"
draft = false

authors = ["Ken Shibata", "twotxh"]
tags = ["Legal"]
+++

# Privacy Policy

Welcome to our Privacy Policy.

Your privacy is critically important to us.

It is the Northern Secondary School Student Affairs Council's policy to respect your privacy regarding any information we may collect while operating our website. This Privacy Policy applies to https://northernsac.ca, Northern Secondary School Student Affairs Council, and/or https://nsssac.gitlab.io (hereinafter, "us", "Northern Secondary School Student Affairs Council", "we", or "https://northernsac.ca" and/or "https://nsssac.gitlab.io"). We respect your privacy and are committed to protecting personally identifiable information you may provide us through the Website. We have adopted this privacy policy ("Privacy Policy") to explain what information may be collected on our Website, how we use this information, and under what circumstances we may disclose the information to third parties. This Privacy Policy applies only to information we collect through the Website and does not apply to our collection of information from other sources.
This Privacy Policy, together with the Terms of Service posted on our Website, set forth the general rules and policies governing your use of our Website. Depending on your activities when visiting our Website, you may be required to agree to additional terms of service.

## Other services
The NSS SAC website may be hosted by GitLab.com, operated by GitLab. The [GitLab Privacy Policy](https://about.gitlab.com/privacy/) may apply on this site.

## Website Visitors

Like most website operators, the NSS SAC collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. The data may be collected using external services such as Google Analytics. The NSS SAC's purpose in collecting non-personally identifying information is to better understand how the NSS SAC's visitors use its website. From time to time, the NSS SAC may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.
The NSS SAC also may collect potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments on https://northernsac.ca or https://nssssac.gitlab.io web pages. The NSS SAC only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.

## Security

The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.

## Links To External Sites

Our Service may contain links to external sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy and terms of service of every site you visit.
We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites, products or services.

## Aggregated Statistics

The NSS SAC may collect statistics about the behavior of visitors to its website. The NSS SAC may display this information publicly or provide it to others. However, the NSS SAC does not disclose your personally-identifying information through publishing aggregated statistics.

## Cookies

To enrich and perfect your online experience, NSS SAC uses "Cookies", similar technologies and services provided by others to display personalized content, appropriate advertising and store your preferences on your computer.
A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. The NSS SAC uses cookies to help the NSS SAC identify and track visitors, their usage of https://northernsac.ca or https://nssac.gitlab.io, and their website access preferences. The NSS SAC visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using the NSS SAC's websites, with the drawback that certain features of the NSS SAC's websites may not function properly without the aid of cookies.
By continuing to navigate our website without changing your cookie settings, you hereby acknowledge and agree to the NSS SAC's use of cookies.

## Privacy Policy Changes

Although most changes are likely to be minor, the NSS SAC may change its Privacy Policy from time to time. The Northern Secondary School administration may also make changes or modifications to these Terms of Use at any time and for any reason. The NSS SAC encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.
