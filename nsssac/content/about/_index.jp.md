+++
title = "About"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata"]
+++

[新しいクラブを始める](http://localhost:1313/jp/faq/#faqs-obj-club-new)
カレンダーにイベントを足したいときは[連絡](/jp/contact)してください。

## Webmasters

2020-2021: [Ken Shibata](https://gitlab.com/colourdelete)

2019-2020: Brandon Fowler

2018-2019: Jori Reiken

2017-2018: Jonathan Katz

## People

{{< objs id="persons" name="People (change this to a more appropriate title)" url="http://gitlab.com/nsssac/data/-/raw/master/persons/persons.json" noGovernance=true noMtgDetails=true noSocials=true >}}
