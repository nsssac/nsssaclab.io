+++
title = "Updates to the SAC Website"
date = "2020-12-22T05:19:16-05:00"
draft = false
summary = "Check out the new updates that we added to the website, including the new Clubs & Associations page."
tags = "Updates"
authors = ["Ken Shibata"]
+++

We added some updates to the website, including updating the [Clubs & Associations page]({{< ref "clubs" >}}) 
with new Associations and adding [a FAQ page]({{< ref "faq" >}}) for common questions and answers with links to other 
helpful websites like the [Northern Secondary School website](https://schoolweb.tdsb.on.ca/northernss/). Also, we changed the layout to make it easier to find the info you want faster.
