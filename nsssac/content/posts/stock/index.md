+++
title = "SAC Stock!"
date = "2021-05-04T15:41:21-05:00"
draft = false
tags = ["SAC", "Stock"]
authors = ["Ken Shibata"]
+++

*The cover image is taken from a {{< instagram "sacnss" >}} post about SAC Stock 2019.*

The SAC Stock is an annual music festival that happens in early June (exact date TBD[^2], but might be June 2[^1]).

All genres of music are accepted but please no inappropriate content (including swearing).

[^1]: https://nsssac.gitlab.io/docs/minutes-senate-2021-05-04/#menu
[^2]: Facebook Messenger messages with Aleksi Toiviainen around 2021-05-04T16:00. "Date tbd"
