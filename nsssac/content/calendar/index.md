+++
title = "Calendar"
date = "1970-01-01T00:00:00"
draft = false
+++

<div class="re-section w3-margin w3-rest" style="padding: 0; height: 600px;">
    <iframe title="Embedded Google Calendar View of School Events in Monthly Format" src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FToronto&amp;src=Y284ZTZkMWZham45N3AwcW9pcDg1ZHJvZHNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=ZW4uY2FuYWRpYW4jaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&amp;src=Y19qcTY1aXJ0cGd1OGMzbGw4bGpiOHFjMXB2MEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23D50000&amp;color=%230B8043&amp;color=%23C0CA33&amp;showTitle=0&amp;showPrint=1&amp;showTabs=1&amp;showCalendars=1&amp;showTz=1" style="margin: 0;" width="100%" height="100%" frameborder="0" scrolling="no"></iframe>
</div>
