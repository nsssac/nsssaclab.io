+++
title = "Final Recommendations of the CRC for the Elections Act (Slideshow Format)"
date = "2021-05-08"
draft = false
tags = ["miscellaneous doc", "object", "doc" , "crc" , "approved senate" , "approved house" , "no-show"]
authors = ["bot"]
presidents = [""]
execs = [""]
staff = [""]
+++

{{< obj id="misc" sub_id="crc-2021-final-elections-slides" url="http://gitlab.com/nsssac/data/-/raw/master/misc/misc.json" noGovernance=true >}}
