+++
title = "Results & Positions"
date = "2021-05-28"
draft = false
tags = ["elections", "elections for 2021-2022", "2020-2021"]
authors = ["Ken Shibata", "Aleksi Toiviainen"]
summary = "Check out the candidates for the Executive and Grade Senators for the 2021-2022 year."
+++

<h3>

[The Epigram](/clubs/clubs-epigram)'s [Review of the Candidates](https://northernepigram.wordpress.com/2021/05/27/meet-the-2021-2022-sac-candidates/)

</h3>

<h3>

[Campaign Videos of All Candidates](https://www.youtube.com/playlist?list=PLJ1PQrQ4sMFewcllBYR9eokabsHC9IvDb)

</h3>

<h3>

[Explanations of Positions](https://www.youtube.com/playlist?list=PLJ1PQrQ4sMFf1-31_M4Se4YvNEtB1-nqP)

</h3>

<table>
<tr>
<th>Position</th>
<th>Candidate(s)</th>
</tr>
<tr>
<td>

#### President

- [Explanation Video](https://www.youtube.com/watch?v=f2eGs59lVBE&list=PLJ1PQrQ4sMFf1-31_M4Se4YvNEtB1-nqP&index=6)
- Ensures the smooth functioning of the Executive
- Chairs Executive meetings
- Represents (or designates representation) for all major school functions
- Leads the SAC Exec in its general activities

</td>
<td>

{{< youtube 9s4j--lUMiU "Finesse Lunsky" >}}

</td>
</tr>
<tr>
<td>

#### Vice President

- [Explanation Video](https://www.youtube.com/watch?v=CbWA73_saEA&list=PLJ1PQrQ4sMFf1-31_M4Se4YvNEtB1-nqP&index=2)
- Acts as the first advisor, first aide, first point of contact for the President
- Chairs the [Senate](/tags/senate)
- Oversees the formation and operation of all groups represented in the [Senate](/tags/senate)
- Chairs the [Elections Committee](/elections/committee)

</td>
<td>

{{< youtube 5k9bLLRz3Nk "Ryan Stainsby" >}}

</td>
</tr>
<tr>
<td>

#### Treasurer

- [Explanation Video](https://www.youtube.com/watch?v=JP978B9Resc&list=PLJ1PQrQ4sMFf1-31_M4Se4YvNEtB1-nqP&index=3)
- Oversees all financial aspects of the SAC, including generated revenue and asset control.
- Makes [budgets](/tags/budgets) (recent budgets are not public)
- Keeps accurate records of SAC assets
- Generates school budgets in collaboration with the exec and key stakeholders

</td>
<td>

{{< youtube 6nQ4aMVbsBc "Sierra Benayon-Abraham" >}}

</td>
</tr>
<tr>
<td>

#### Secretary

- [Explanation Video](https://www.youtube.com/watch?v=QxOq_-Jf1UA&list=PLJ1PQrQ4sMFf1-31_M4Se4YvNEtB1-nqP&index=6)
- Maintains the order, coherence, and availability of all SAC legislation, schedules, internal documents,
  and other written material
- Writes notes of courtesy, "thank you", "congratulations", and the like
- Takes attendance and [minutes](/tags/minutes) at all meetings

</td>
<td>

{{< youtube AF53NyARuoc "Eden Grosssman" >}}

</td>
</tr>
<tr>
<td>

#### External Social Director

- [Explanation Video](https://www.youtube.com/watch?v=2vbQeepFbDU&list=PLJ1PQrQ4sMFf1-31_M4Se4YvNEtB1-nqP&index=1)
- Researches, plans, and implements all activities which take place outside of school hours.
  These include formals, semi-formals, and graduation events.
- May also help the [Internal Social Director](#internal-social-director).
- Ensure, with the collaboration of the [Treasurer](#treasurer), all proceeds from external social events are deposited in the SAC account

</td>
<td>

{{< youtube ZKHvkk9NVl4 "Zoey Wallhouse" >}}

</td>
</tr>
<tr>
<td>

#### Internal Social Director

- [Explanation Video](https://www.youtube.com/watch?v=2xuj8iAZXkY&list=PLJ1PQrQ4sMFf1-31_M4Se4YvNEtB1-nqP&index=4)
- Researches, plans, and implements all activities which take place inside of school hours.
  These include spirit days and carnivals
- May also help the [External Social Director](#external-social-director).
- Ensure, in collaboration with the [Treasurer](#treasurer), that all proceeds from the internal social events are deposited in the SAC account

</td>
<td>

{{< youtube vd8Uj0FJMo0 "Emily Gluckman" >}}

</td>
</tr>
<tr>
<td>


#### Grade 10 Senator

- No Explanation Video
- Represent the Grade 10 Student Body in the [Senate](/tags/senate) and [House of Reps](/tags/house)
- Oversee the concerns of the Grade 10 Class Representatives during [House of Reps](/tags/house) meetings

</td>
<td>

{{< youtube k-nSN7Az2d8 "Astoria Yen" >}}

</td>
</tr>
<tr>
<td>


#### Grade 11 Senator

- No Explanation Video
- Represent the Grade 11 Student Body in the [Senate](/tags/senate) and [House of Reps](/tags/house)
- Oversee the concerns of the Grade 11 Class Representatives during [House of Reps](/tags/house) meetings

</td>
<td>

{{< youtube dFkL5UwwloY "Ben Hopkins" >}}

</td>
</tr>
<tr>
<td>


#### Grade 12 Senator

- [Explanation Video](https://www.youtube.com/watch?v=3UCIhnJY3NQ&list=PLJ1PQrQ4sMFf1-31_M4Se4YvNEtB1-nqP&index=8)
- Represent the Grade 12 Student Body in the [Senate](/tags/senate) and [House of Reps](/tags/house)
- Oversee the concerns of the Grade 12 Class Representatives during [House of Reps](/tags/house) meetings

</td>
<td>

{{< youtube m7kEu_AQvzk "Joseph Kopun" >}}

</td>
</tr>
</table>
