+++
title = "Elections Committee"
date = "2021-05-28"
draft = false
tags = ["elections"]
authors = ["Ken Shibata"]
+++

The Elections Committee manages the election process for a given school year.

It is chaired by the Vice President.
