+++
title = "ノーザン生徒委員会"
date = "1970-01-01T00:00:00"
draft = false

authors = ["Ken Shibata"]
tags = ["Home"]
+++

{{< days "https://gitlab.com/nsssac/data/-/raw/master/days.json" >}}

このウェブサイトには学校の[イベント](/events)や[生徒委員会](/about)、[クラブ](/clubs)の情報があります。
