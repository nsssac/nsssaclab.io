+++
title = "Day Schedule"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata"]
+++

![Day schedule diagram provided by the TDSB](https://lh6.googleusercontent.com/fdQCmGL5AMhfmD2MVtKpm8UguYAHYltXZ5gcBAq717Nd369rLFzymhjJplIzEuXyx0BzJ3qdi010k7Oy0H4D1elmieNJ_Zb65YbiIuggiFLpJRhsjTePEk1kz7gIrDPQew)

# Morning

Note: Times are in 24 hour format.

Note: on *Early Closing* days, times may be changed from what is shown below. Early Closing days may have a "(Early Closing)" event on the [School Events Calendar](/events).

## `08:35` *Classrooms are open*

- Teachers are present in classrooms

## `08:40` *10 minute warning bell*

- NSS Air music selections
- Students proceed to their period 1 classrooms

## `08:45` *5 minute warning bell*

- Students should be in their period 1 classrooms

## `08:50` *Period 1 begins*

- Students not in class are marked late
- Students are marked late will be counselled by their period 1 teacher
    - If a pattern of lateness develops, students will be referred to their Vice-Principal

## `12:30` *Period 1 ends*

- Students may exit from their appropriate door when the announcement for their floor (or for floor 3 for floor 4) is made.

## `12:30` *Period 2 begins*

## `15:15` *Period 2 ends*

# Signing In / Signing Out

Note: Appointments should be made after school hours whenever possible. Students who wish to be excused during the day must observe the following procedures:
• Students must present a note from their parent/guardian to the Vice-Principal Office prior to leaving.
• Students who wish to go home during the school day because of illness must sign out in the VP’s office. Students may not leave until a parent or guardian has been contacted.
• If a student goes home for lunch and is unable to return, parents/guardians must contact the school by phone or email.
• Students returning from an appointment must sign in at the VP office.
