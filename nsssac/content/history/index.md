+++
title = "History"
date = 2021-03-11T20:40:13.000Z
draft = false
tags = ["History"]
authors = ["Ken Shibata", "Unknown", "Aleksi Toiviainen"]
+++

## 1930

Northern Secondary School was founded in 1930 as Northern Vocational School. In 1958, it became Northern Secondary School, and it has stayed as such. NSS was the first school in Ontario to have a student council, as well as the first school to institute a deaf/hard of hearing program. 500 Northern students served in World War II, with 150 giving their lives for the cause. Since 2009,  

![Nathan Leipciger speaking to students at a Holocaust Education Week event (2013)](nsssac-holocaust-week-event-2013.png "Nathan Leipciger speaking to students at a Holocaust Education Week event (2013)")

Northern has regularly had Holocaust survivors speak at assemblies, encouraging students to be politically active and never forget the atrocities committed before and during the second world war.

## 2000

In 2009, Northern teacher Dr. Danielle Gauci joined an Arctic research expedition to study climate change. The trip was two and a half weeks long, and was run by University of Manitoba's Schools on Board initiative, an initiative designed to increase interest in Arctic research and climate change study in Canada. 

## 2010

In 2010, Northern’s SmartRisk team was awarded $6,000 to attend the Annual National Service-Learning Conference in California. They had a booth showcasing their work, and organized an activity promoting teen driver safety awareness. Also that year, Northern student Madeleine Davidson competed in the IAAF World Cross Country Championships in Bydgoszcz, Poland, finishing in 75th place. 

## 2011

Work on the Clarke Pulford Field began in 2011. This field cost $1.1 million and replaced the natural grass field with astroturf, which is far easier and safer for athletes to play sports on. 

![(Left) Northern teacher and coach Clarke Pulford and students celebrate groundbreaking of the Clarke Pulford Field (2011)](nsssac-clarke-pulford-field-groundbreaking.png "(Left) Northern teacher and coach Clarke Pulford and students celebrate groundbreaking of the Clarke Pulford Field (2011)")

In 2012, Northern students wrote and performed The Masque of Beauty and the Beast, which would go on to be performed at the Sears Ontario Drama Festival, a province-wide theatre competition and showcase. Also that year, Northern alumnus Matt Black came back to Northern with the recently won Grey Cup. He won playing with the Toronto Argonauts, against the Calgary Stampeders. 

## 2013

NSS United Way club won the United Way’s Spirit Award in 2013 for the second time in a row, and the tenth time since 1996, after raising $34,000. Later that year, NSS United Way fundraised $43,200, shattering its own record. 

## 2014

The Northern SS Red Knights junior football team had a perfect season in 2014. Running back Daniel Adeboboye stood out with thirty touchdowns over eight games. The year after, Northern put on its first musical in eight years: John Waters’ Hairspray. It was called the largest production in Northern history, and took around a year of planning and preparation.

![Northern students performing Hairspray (2015)](nsssac-hairspray-2015.png "Northern students performing Hairspray (2015)")

## 2017

A learning garden was unveiled in 2017, with the ultimate goal of providing food for families in need. Northern freshman Mikayla Crawford won a gold medal for snowboarding at OFSAA, and the year after the Northern girl’s snowboarding team won four medals at OFSAA, as well as Northern’s hockey team winning the city’s first-ever junior hockey title. 

## 2020

In 2020, Northern mourned the loss of Maya Zibaie, a tenth grade student who passed away in the Ukraine International Airlines Flight 752 crash. Maya spent her time supporting other ESL learners, and worked through her Grade 9 ESL class, moving on to take Grade 10 English. 

Today, Northern is a vibrant and community-oriented school, with many clubs, activities, and ways for students to get involved. We are persevering through COVID-19 and supporting each other through this year, as we will continue to do in years to come.
