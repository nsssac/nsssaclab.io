+++
title = "クラブ"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata"]
+++

## [新しいクラブを始める](/faq#faqs-obj-club-new)

[クラブのスプレッドシート（CSV、自動生成）](https://gitlab.com/nsssac/data/-/jobs/artifacts/master/raw/clubs.csv?job=fmt:to_csv)

ここでは、クラブやクラブの役員の申請書とクラブの申請書が出てきたときに見つけることができます。
また、年末になると詳細や正確な締め切りがでてきます。
