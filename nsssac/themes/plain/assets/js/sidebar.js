const sidenav = document.getElementById('sidenav');
const sidenavOpen = document.getElementById('sidenav-open');
const sidenavClose = document.getElementById('sidenav-close');
const content = document.getElementById('content');
const coverImg = document.getElementById('cover-img');
const sidefloat = document.getElementById('sidefloat');
const sidefloatIcon = document.getElementById('sidefloat-icon');

/* Nav (#sidenav) */

function navSet(name, open, close, node) {
    let section = document.getElementById("sidenav-section");
    section.style.display = "none";
    all = document.getElementsByClassName('topnav-content');
    for (let i = 0; i < all.length; i++) {
        all[i].style.display = name;
    }
    sidenavOpen.style.display = open;
    sidenavClose.style.display = close;
    const nodes = sidenav.childNodes;
    for(let i=0; i<nodes.length; i++) {
        if (nodes[i].nodeName.toLowerCase() === "section" && nodes[i].id !== "sidenav-section") {
            nodes[i].style.display = node;
        }
    }
}

function navOpen() {
    navSet("block", "none", "block", "none")
}

function navClose() {
    navSet("none", "block", "none", "block")
}

/* Float (#sidefloat) */

function floatSet(nav, cont, img, state, onclick) {
    sidenav.style.left = nav;

    content.style.marginLeft = cont;
    if (coverImg !== null) coverImg.style.marginLeft = img;
    if (state) {
        sidefloatIcon.style.transform = "rotateX(90deg)";
    } else {
        sidefloatIcon.style.transform = "rotateX(0)";
    }
    sidefloat.onclick = onclick;
}

function floatShow() {
    floatSet("0", "22em", "20em", false, floatHide)
}

function floatHide() {
    floatSet("-20em", "2em", "0", true, floatShow)
}
