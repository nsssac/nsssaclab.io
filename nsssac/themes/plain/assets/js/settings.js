let hoverDetailsOpen = false;

// add details hover listener
document.querySelectorAll('details').forEach(node => {
    node.addEventListener("mouseenter", () => {
        if (hoverDetailsOpen) {
            node.open = true
        }
    })
    node.addEventListener("mouseleave", () => {
        if (hoverDetailsOpen) {
            node.open = false
        }
    })
})

if (localStorage.getItem("hover-details-open") !== null) {
    hoverDetailsOpen = localStorage.getItem("hover-details-open") === "yes";
}

const select = document.getElementById("select-theme");
function updateTheme() {
    let text = select.selectedIndex;
    if (text === 0) {
        setThemeLayout('light', null);
    } else if (text === 1) {
        setThemeLayout('dark', null);
    }
}

if (localStorage.getItem("theme") === "light") {
    select.selectedIndex = 0
} else if (localStorage.getItem("theme") === "dark") {
    select.selectedIndex = 1
}
updateTheme();

if (localStorage.getItem("anim-delay") !== null) {
    document.getElementById('anim-delay-input').value = localStorage.getItem("anim-delay");
} else {
    document.getElementById('anim-delay-input').value = document.documentElement.style.getPropertyValue('--anim-delay');
}

function setAnimDelay(animDelay) {
    if (animDelay === null) {
        animDelay = document.getElementById('anim-delay-input').value;
    }
    document.documentElement.style.setProperty('--anim-delay', animDelay);
    localStorage.setItem("anim-delay", animDelay);
}
