let menuOpen = false;
const menu = document.getElementById("menu");
const toc = document.getElementById("TableOfContents");

function toggleMenu() {
    menuOpen = !menuOpen;
    if (menuOpen) {
        menu.style.left = "0";
        history.pushState(null, null, '#')
    } else {
        menu.style.left = "-100vw";
        history.replaceState(null, null, '#')
    }
}

if (toc !== null) {
    toc.querySelectorAll("a").forEach(node => node.onclick = toggleMenu);
}

if (window.location.hash) {
    let selected = document.getElementById(window.location.hash);
    if (selected) {
        if (selected.textContent.length <= 100) {
            window.title = window.title + ": " + selected.textContent;
        }
        if (selected.id === "menu") {
            menuOpen = true; // ...#menu should open the menu, and js should know that too
        }
    }
}
