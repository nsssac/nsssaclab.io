convert "$1" "$(echo "$1" | rev | cut -f 2- -d . | rev).avif" &
convert "$1" "$1.avif" &
convert "$1" "$1.webp" &
convert "$1" "$1.jxl" &
