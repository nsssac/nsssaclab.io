#!/bin/sh

if [ -d "$CACHE_FLAG" ]; then
    echo "$CACHE_FLAG exists; Hugo built."
    export PATH=$PWD/bin:$GOPATH/bin:$PATH
else
    echo "$CACHE_FLAG does not exist; Hugo not built."
    sh '.gitlab/ci/before/hugo.sh'
    export PATH=$PWD/bin:$GOPATH/bin:$PATH
fi
hugo version
