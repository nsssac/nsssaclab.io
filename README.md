# (Beta) Northern SAC Website

[Data](https://gitlab.com/nsssac/data)

[Live Site](https://nsssac.gitlab.io) |
[Critical Issues/MRs](https://gitlab.com/nsssac/nsssac.gitlab.io/-/boards/2284314?&label_name[]=confirmed&label_name[]=priority%3A%3A0) |
[Issues/MRs](https://gitlab.com/nsssac/nsssac.gitlab.io/-/boards/2284311) |

## Build Locally

Please clone [https://gitlab.com/nsssac/nsssac.gitlab.io](the Git repo for this site) and run `hugo` (or `hugo --minify`) in the repo (with dependencies). You may need an internet connection, as pages such as the FAQ, Clubs, Minutes, etc are generated from a JSON file which is downloaded from [https://gitlab.com/nsssac/data](another Git repo). To circumvent this, you must download the files and change the source to use the downloaded files instead. We might change the CI pipeline to download the files pre running Hugo, so the process described before is easier to do (no source edits).

```bash
git clone https://gitlab.com/nsssac/nsssac.gitlab.io.git
cd nsssac.gitlab.io
hugo --minify
```

Visit `./public/index.html` to visit the index.

## Testing Command

```bash
git clone https://gitlab.com/nsssac/nsssac.gitlab.io.git
cd nsssac.gitlab.io
hugo -v server --path-warnings --ignoreCache --disableFastRender --renderToDisk --cleanDestinationDir
```

## TODO

- [ ] Release
    - [x] Redirect some pages (e.g. clubs) to new site
    - [ ] Auto-redirect to new site and show banner to allow moving back to northernsac.ca
    - [ ] Remove auto-redirect
- [ ] Data
    - [x] For `<table/>`s, change colour of the alternating rows
    - [ ] Obfuscate names (no names in data nor website)
    - [ ] move all `northernsac.ca` links to `gitlab.com/nsssac/data` links
